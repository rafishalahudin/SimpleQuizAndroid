package com.blogspot.rafimuhamadshalahudin.finalproject;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    double skor = 0;
    RadioButton a1, b2, b4, a5, a6, a7, a8, a9, a10;
    CheckBox a3, b3, c3;
    EditText namaAnda, ed9;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        a1 = (RadioButton) findViewById(R.id.a1);
        b2 = (RadioButton) findViewById(R.id.b2);
        a3 = (CheckBox) findViewById(R.id.a3);
        b3 = (CheckBox) findViewById(R.id.b3);

        c3 = (CheckBox) findViewById(R.id.c3);
        b4 = (RadioButton) findViewById(R.id.b4);
        a5 = (RadioButton) findViewById(R.id.a5);
        a6 = (RadioButton) findViewById(R.id.a6);
        a7 = (RadioButton) findViewById(R.id.a7);
        a8 = (RadioButton) findViewById(R.id.a8);
//        a9 = (RadioButton)findViewById(R.id.a9);
        a10 = (RadioButton) findViewById(R.id.a10);
        ed9 = (EditText) findViewById(R.id.ed9);


        namaAnda = (EditText) findViewById(R.id.namaAnda);

        Button b = (Button) findViewById(R.id.btnsubmit);

        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (a1.isChecked()) {
                    skor++;
                }
                if (b2.isChecked()) {
                    skor++;
                }
                if (a3.isChecked()) {
                    skor += 0.5;
                }
                if (b3.isChecked()) {
                    skor -= 0.5;
                }
                if (c3.isChecked()) {
                    skor += 0.5;
                }
                if (b4.isChecked()) {
                    skor++;
                }
                if (a5.isChecked()) {
                    skor++;
                }
                if (a6.isChecked()) {
                    skor++;
                }
                if (a7.isChecked()) {
                    skor++;
                }
                if (a8.isChecked()) {
                    skor++;
                }
                if (ed9.getText().toString().toUpperCase().trim().equals("SSD")) {
                    skor++;
                }
                if (a10.isChecked()) {
                    skor++;
                }



                String nama = namaAnda.getText().toString();

                if(nama.isEmpty()){
                    Toast.makeText(MainActivity.this, "Anda tidak mengisi nama", Toast.LENGTH_SHORT).show();
                }else if (skor == 0) {
                    Toast t = Toast.makeText(MainActivity.this,"Anda tidak mengisi jawaban",Toast.LENGTH_LONG);
                    t.show();
                }
                else{
                    Intent i = new Intent(MainActivity.this, Main2Activity.class);
                    i.putExtra("pesan", skor + "");
                    i.putExtra("nama", nama);
                    i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                    startActivity(i);
                }



                skor = 0;
            }
        });

    }

    public void onRestart() {
        super.onRestart();
        Intent intent = getIntent();
        finish();
        startActivity(intent);
    }
}
