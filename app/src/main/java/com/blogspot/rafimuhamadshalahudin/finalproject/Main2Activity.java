package com.blogspot.rafimuhamadshalahudin.finalproject;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class Main2Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        TextView tv = (TextView)findViewById(R.id.tv);
        EditText ed = (EditText)findViewById(R.id.namaAnda);
        Button rs = (Button) findViewById(R.id.intenPage3);
        rs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Main2Activity.this, Main3Activity.class);
                startActivity(i);
            }
        });

        Intent intent = getIntent();
        Bundle extras = getIntent().getExtras();

        String message = extras.getString("pesan");
        String nama = extras.getString("nama");
        tv.setText(nama+"\nSkor Anda : "+message);
    }
}
